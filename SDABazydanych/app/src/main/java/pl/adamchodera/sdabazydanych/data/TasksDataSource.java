/*
 * Copyright 2016, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.adamchodera.sdabazydanych.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

/**
 * Concrete implementation of a data source as a db.
 */
public class TasksDataSource {

    private static TasksDataSource INSTANCE;

    private DatabaseHelper databaseHelper;

    // Prevent direct instantiation
    private TasksDataSource(@NonNull Context context) {
        databaseHelper = new DatabaseHelper(context);
    }

    public static synchronized TasksDataSource getInstance(@NonNull Context context) {
        // Use the application context, which will ensure that you don't accidentally leak an Activity's context.
        // See this article for more information:
        // http://www.androiddesignpatterns.com/2012/05/correctly-managing-your-sqlite-database.html
        if (INSTANCE == null) {
            INSTANCE = new TasksDataSource(context.getApplicationContext());
        }
        return INSTANCE;
    }

    public void closeDatabase() {
        databaseHelper.close();
    }

    public long saveTask(@NonNull TaskEntity task) {

        // TODO save task in database

        // TODO return taskId instead of -1
        return -1;
    }

    public List<TaskEntity> getNotCompletedTasks() {
        List<TaskEntity> tasks = new ArrayList<>();

        // TODO get tasks from DB which are not completed

        return tasks;
    }

    public TaskEntity getTask(@NonNull long taskId) {
        SQLiteDatabase db = databaseHelper.getReadableDatabase();

        // TODO get one task by taskId

        return null;
    }

    public void updateTask(@NonNull TaskEntity task) {

        // TODO update task in database

    }

    public void completeTask(@NonNull TaskEntity task) {

        // TODO set particular task as completed, using its ID

    }


    // below are additional, not yet implemented methods
    // you can use them when once you implement more functions in the app

    public void activateTask(@NonNull TaskEntity task) {
        SQLiteDatabase db = databaseHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(TodoContract.TaskEntry.COLUMN_NAME_COMPLETED, false);

        String selection = TodoContract.TaskEntry._ID + " = ?";
        String[] selectionArgs = {task.getStringId()};

        db.update(TodoContract.TaskEntry.TABLE_NAME, values, selection, selectionArgs);
    }

    public void clearCompletedTasks() {
        SQLiteDatabase db = databaseHelper.getWritableDatabase();

        String selection = TodoContract.TaskEntry.COLUMN_NAME_COMPLETED + " = ?";
        String[] selectionArgs = {"1"};

        db.delete(TodoContract.TaskEntry.TABLE_NAME, selection, selectionArgs);
    }

    public void deleteAllTasks() {
        SQLiteDatabase db = databaseHelper.getWritableDatabase();

        db.delete(TodoContract.TaskEntry.TABLE_NAME, null, null);
    }

    public void deleteTask(@NonNull String taskId) {
        SQLiteDatabase db = databaseHelper.getWritableDatabase();

        String selection = TodoContract.TaskEntry._ID + " = ?";
        String[] selectionArgs = {taskId};

        db.delete(TodoContract.TaskEntry.TABLE_NAME, selection, selectionArgs);
    }
}
